import express from "express";
import authenticate from "../../src/infrastructure/middleware/authenticate";
import expanseController from "../../src/infrastructure/controller/expanse";

const router = express.Router();

router.post('add', expanseController.postAdd);

router.post('cancel', expanseController.postCancel);

export default router;