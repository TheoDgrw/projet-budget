import express from "express";
import authenticate from "../../src/infrastructure/middleware/authenticate";
import expanseRoutes from "./expanse";

const router = express.Router();

router.use('/expanse', authenticate, expanseRoutes);