const express = require("express");
const bodyParser = require("body-parser");

const app = express();


class App {

    constructor() {
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        
        this.launch();
    }

    launch() {
        app.listen(4000, () => {
            console.log('listening on port 4000');
        });
    }
}