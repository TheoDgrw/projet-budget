import { NextFunction, Request, Response } from "express";
import { getManager } from "typeorm";
import { User } from "../entity/User";


export default async function authenticate(req: Request, res: Response, next: NextFunction) {
    const manager = getManager();
    res.locals.user = await manager.findOne(User, {name: req.body.user});

    if (!res.locals.user) {
        res.json({
            errors: [
                {
                    flashMessage: 'user not found'
                }
            ]
        })
    } else {
        next();
    }
}
