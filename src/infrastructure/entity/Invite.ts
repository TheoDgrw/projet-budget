import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import IInfraEntity from "../../domain/entity/iinfraEntity";
import { Group } from "./Group";
import { User } from "./User";

@Entity()
export class Invite implements IInfraEntity {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _accepted: boolean;
    private _user: User;
    private _group: Group;


    @Column({default: false})
    public get accepted(): boolean {
        return this._accepted;
    }
    public set accepted(accepted: boolean) {
        this._accepted = accepted;
    }

    @ManyToOne(() => User, user => user.invites)
    public get user(): User {
        return this._user;
    }
    public set user(user: User) {
        this._user = user;
    }

    @ManyToOne(() => Group, group => group.invites)
    public get group(): Group {
        return this._group;
    }
    public set group(group: Group) {
        this._group = group;
    }
}
