import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import IInfraEntity from "../../domain/entity/iinfraEntity";
import { Group } from "../entity/group";
import { User } from "../entity/user"; 

@Entity()
export class Expanse implements IInfraEntity {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _amount: number;
    private _reimbursed: number;
    private _group: Group;
    private _user: User;

    @Column()
    public get amount(): number {
        return this._amount;
    }
    public set amount(amount: number) {
        this._amount = amount;
    }

    @Column()
    public get reimbursed(): number {
        return this._reimbursed;
    }
    public set reimbursed(reimbursed: number) {
        this._reimbursed = reimbursed;
    }

    @ManyToOne(() => Group, group => group.expanses)
    public get group(): Group {
        return this._group;
    }
    public set group(group: Group) {
        this._group = group;
    }

    @ManyToOne(() => User, user => user.expanses)
    public get user(): User {
        return this._user;
    }
    public set user(user: User) {
        this._user = user;
    }
    
}
