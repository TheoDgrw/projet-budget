import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, OneToMany, JoinTable} from "typeorm";
import IInfraEntity from "../../domain/entity/iinfraEntity";
import { Expanse } from "../entity/expanse";
import { Invite } from "../entity/invite";

@Entity()
export class User implements IInfraEntity {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _name: string;
    private _invites: Invite[];
    private _users: Expanse[];
    private _expanses: Expanse[];


    @Column()
    public get name(): string {
        return this._name;
    }
    public set name(name: string) {
        this._name = name;
    }

    @OneToMany(() => Invite, invite => invite.user)
    public get invites(): Invite[] {
        return this._invites;
    }
    public set invites(invites: Invite[]) {
        this._invites = invites;
    }

    @ManyToMany(() => Expanse)
    @JoinTable()
    public get users(): Expanse[] {
        return this._users;
    }
    public set users(users: Expanse[]) {
        this._users = users;
    }

    @OneToMany(() => Expanse, expanse => expanse.user)
    public get expanses(): Expanse[] {
        return this._expanses;
    }
    public set expanses(expanses: Expanse[]) {
        this._expanses = expanses;
    }

}
