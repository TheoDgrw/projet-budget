import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import IInfraEntity from "../../domain/entity/iinfraEntity";
import { Expanse } from "../entity/expanse";
import { Invite } from "../entity/invite";

@Entity()
export class Group implements IInfraEntity {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _name: string;
    private _expanses: Expanse[]
    private _invites: Invite[]

    @Column()
    public get name(): string {
        return this._name;
    }
    public set name(name: string) {
        this._name = name;
    }

    @OneToMany(() => Expanse, expanse => expanse.group)
    public get expanses(): Expanse[] {
        return this._expanses;
    }
    public set expanses(expanses: Expanse[]) {
        this._expanses = expanses;
    }

    @OneToMany(() => Invite, invite => invite.group)
    public get invites(): Invite[] {
        return this._invites;
    }
    public set invites(invites: Invite[]) {
        this._invites = invites;
    }
    
}
