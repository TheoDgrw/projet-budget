import {MigrationInterface, QueryRunner} from "typeorm";

export class first1611070439136 implements MigrationInterface {
    name = 'first1611070439136'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "invite"."accepted" IS NULL`);
        await queryRunner.query(`ALTER TABLE "invite" ALTER COLUMN "accepted" SET DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "invite" ALTER COLUMN "accepted" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "invite"."accepted" IS NULL`);
    }

}
