import { EntityRepository, Repository, getManager } from "typeorm";
import IGroupRepository from "../../domain/repository/igroup";
import Group from "../../domain/entity/group";
import Invite from "../../domain/entity/invite";
import User from "../../domain/entity/user";



@EntityRepository(Group)
export default class GroupRepository extends Repository<Group> {

    async join(user: User, group: Group): Promise<boolean> {

        const invite = await this.manager.create(Invite, {
            accepted: true,
            user: user,
            group: group
        });

        return invite ? true : false;
    }

    async create(name: string, users: User[], amount: number) {

    }
}