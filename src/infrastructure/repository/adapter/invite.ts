import Group from "../../../domain/entity/group";
import Invite from "../../../domain/entity/invite";
import User from "../../../domain/entity/user";
import IInviteRepository from "../../../domain/repository/iinvite";
import { Invite as TypeormInvite } from "../../entity/invite";
import GroupRepositoryAdapter from "./group";



export default class InviteRepositoryAdapter implements IInviteRepository {

    static infraToDomain(invite: TypeormInvite, group: Group, user: User): Invite {

        return new Invite(invite.accepted, user, group);
    }

    accept(user: User) {

    }

}