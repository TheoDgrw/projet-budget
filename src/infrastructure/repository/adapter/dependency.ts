// import Expanse from "../../../domain/entity/expanse";
// import Group from "../../../domain/entity/group";
// import Invite from "../../../domain/entity/invite";
// import User from "../../../domain/entity/user";
// import { Expanse as TypeormExpanse } from "../../entity/expanse";
// import { Group as TypeormGroup } from "../../entity/group";
// import { Invite as TypeormInvite } from "../../entity/invite";
// import { User as TypeormUser } from "../../entity/User";
import * as typeormEntities from "../../entity";
import * as entities from "../../../domain/entity";
import IDomainEntity from "../../../domain/entity/idomainEntity";
import IInfraEntity from "../../../domain/entity/iinfraEntity";

class DependencyAdapter {

    private create(typeormEntity: IInfraEntity, relation: IInfraEntity[] | IInfraEntity[][], parentDomain: IDomainEntity | null = null, depth: number = 0) {
        const attributes = {};
        const properties: Array<any> = [];
        const relations = [];

        for (let attr in typeormEntity) {
            const localAttr = typeormEntity[attr];

            for(let entityClassAttr in typeormEntities) {

                if (localAttr instanceof typeormEntities[entityClassAttr]
                    || (Array.isArray(localAttr)
                        && localAttr[0]
                        && localAttr[0] instanceof typeormEntities[entityClassAttr]
                    )
                ) {

                    relations.push(typeormEntity[entityClassAttr]);

                } else {

                    attributes[localAttr] = typeormEntity[localAttr];
                }
            }
        }

        const entity = new entities[typeormEntity.constructor.name](attributes);

        if (!parentDomain !== null) {
            entity[parentDomain.constructor.name] = parentDomain;
        }

        relations.forEach(relation => {

            this.create(typeormEntity[relation.constructor.name], relations, entity);
        });

        return entity;

        // for(const key in Object.keys(typeormEntities)) {

        //     attr = this._typeormEntity
        //     if (attr instanceof typeormEntities[key]) {

        //         const classes = attr.getClass();

        //         relations.push(classes);

        //     } else if (Array.isArray(attr) && attr[0] instanceof TypeormEntity) {
                
        //     } else {
        //         properties.push(property);
        //         attributes.push(attr);
        //     }
        // }

        // const localClasses = this._typeormEntity.getClass();
        // const obj = {};
        // attributes.forEach((attribute, i) => {
        //     obj[properties[i]] = attribute;
        // });

        // new localClasses.domain(obj);


    }
}