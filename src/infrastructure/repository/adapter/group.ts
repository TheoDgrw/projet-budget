import { EntityManager, getConnection } from "typeorm";

import Group from "../../../domain/entity/group";
import User from "../../../domain/entity/user";
import Invite from "../../../domain/entity/invite";
import IGroupRepository from "../../../domain/repository/igroup";

import {Group as TypeormGroup} from "../../entity/Group";
import { Invite as TypeormInvite } from "../../entity/invite";
import GroupRepository from "../group";

import InviteRepositoryAdapter from "./invite";



export default class GroupRepositoryAdapter implements IGroupRepository {

    constructor(
        private repo: GroupRepository,
        private manager: EntityManager
    ) {}

    join(user: User, group: Group) {
        getConnection()
            .createQueryBuilder()
            .relation(TypeormGroup, 'invites')
            .of(group)
            .add(user)
        ;
    }

    create(name: string, users: User[], amount: number) {
        getConnection()
            .createQueryBuilder()
            .insert()
            .into(TypeormGroup)
            .values({ name: name })
            .execute()
        ;
    }

    async findAll(user: User): Promise<Group> {
        const groups: TypeormGroup[] = await getConnection()
            .createQueryBuilder()
            .select('group')
            .from(TypeormGroup, 'group')
            .where('group.invite.id = :name', { name: user.name })
            .getMany()
        ;

         groups.map(group => {
            const newGroup =  new Group(group.name);
            newGroup.invites = group.invites.map(invite => {
                const newUser = new User(invite.user.name, [], [], [])
                newUser.
                new Invite(invite.accepted)
            })
        });
    }

    test(entity: any) {
        
    }
}