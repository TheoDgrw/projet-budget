import { EntityRepository, Repository } from "typeorm";
import { Expanse } from "../entity/Expanse";
import { User } from "../entity/User";



@EntityRepository(Expanse)
export default class ExpanseRepository extends Repository<Expanse> {

}