import Expanse from "../entity/expanse";
import User from "../entity/user";



export default interface IExpanseRepository {

    add(amount: number, users: User[]): void;

    cancel(expanse: Expanse): void;

}