import Group from "../entity/group";
import User from "../entity/user";



export default interface IGroupRepository {

    create(name: string, users: User[], amount: number): void;

    join(user: User, group: Group): void;
    
    findAll(user: User): User[] | Promise<User[]>;
    
}