import Invite from "../entity/invite";
import User from "../entity/user";



export default interface IInviteRepository {

    accept(user: User): void;

}