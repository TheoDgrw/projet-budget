import Expanse from "./expanse";
import Invite from "./invite";
import IDomainEntity from "./idomainEntity"


export default class Group implements IDomainEntity {

    public readonly id: number;

    private _name: string;
    private _expanses: Expanse[];
    private _invites: Invite[];

    constructor(obj: any) {
        this.id = obj.id;
        this._name = obj.name;
    }

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get expanses(): Expanse[] {
        return this._expanses;
    }

    public set expanses(expanses: Expanse[]) {
        this._expanses = expanses;
    }

    public get invites(): Invite[] {
        return this._invites;
    }

    public set invites(invites: Invite[]) {
        this._invites = invites;
    }

    
}
