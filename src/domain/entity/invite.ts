import User from "./user";
import Group from "./group";
import IDomainEntity from "./idomainEntity";


export default class Invite implements IDomainEntity {

    public readonly id: number;

    private _accepted: boolean = false;
    private _user: User;
    private _group: Group;

    constructor(obj: any) {
        this._accepted = obj.accepted;
    }

    public get accepted(): boolean {
        return this._accepted;
    }

    public set accepted(accepted: boolean) {
        this._accepted = accepted;
    }

    public get user(): User {
        return this._user;
    }

    public set user(user: User) {
        this._user = user;
    }

    public get group(): Group {
        return this._group;
    }

    public set group(group: Group) {
        this._group = group;
    }


}
