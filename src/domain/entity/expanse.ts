import Group from "./group";
import User from "./user";
import IDomainEntity from "./idomainEntity";


export default class Expanse implements IDomainEntity {

    public readonly id: number;

    private _amount: number;
    private _reimbursed: number;
    private _group: Group;
    private _user: User;

    constructor(obj: any) {
        this._amount = obj.amount;
        this._reimbursed = obj.reimbursed;
    }

    public get amount(): number {
        return this._amount;
    }

    public set amount(amount: number) {
        this._amount = amount;
    }

    public get reimbursed(): number {
        return this._reimbursed;
    }

    public set reimbursed(reimbursed: number) {
        this._reimbursed = reimbursed;
    }

    public get group(): Group {
        return this._group;
    }

    public set group(group: Group) {
        this._group = group;
    }

    public get user(): User {
        return this._user;
    }

    public set user(user: User) {
        this._user = user;
    }

    
}
