import Invite from "./invite";
import Expanse from "./expanse";
import IDomainEntity from "./idomainEntity";


export default class User implements IDomainEntity {

    public readonly id: number;

    private _name: string;
    private _invites: Invite[];
    private _users: Expanse[];
    private _expanses: Expanse[];

    constructor(obj: any) {
        this.id = obj.id;
        this._name = obj.name;
    }

    public get name(): string {
        return this._name;
    }

    public set name(name: string) {
        this._name = name;
    }

    public get invites(): Invite[] {
        return this._invites;
    }

    public set invites(invites: Invite[]) {
        this._invites = invites;
    }

    public get users(): Expanse[] {
        return this._users;
    }

    public set users(users: Expanse[]) {
        this._users = users;
    }

    public get expanses(): Expanse[] {
        return this._expanses;
    }

    public set expanses(expanses: Expanse[]) {
        this._expanses = expanses;
    }


}
